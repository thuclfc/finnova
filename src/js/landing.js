$(window).on('load',function () {
   $('body').addClass('page_load');

   var num_slide = $('.item_slide').length;
   var height_slide = $('.item_slide:not(.active)').height();
   var index_active = $('.item_slide.active').index() + 1;
    if(index_active == 1){
        $('.btn-prev').off('click').addClass('disable');
    }else if(index_active >= num_slide){
      $('.btn-next').off('click');
    }

   var i = 1;
   $('.btn-next').on('click',function () {
       var next_slide = index_active + 1;
       var movie_slide = 0 - (index_active * height_slide);
       if(index_active >= num_slide){
           $(this).addClass('disable');
           return false;
       }else if(index_active >= 1){
           $('.btn-prev').removeClass('disable');
       }
       $('.slide_home_container').addClass('selected');
       $('.item_slide').removeClass('active');
       setTimeout(function () {
           $('.slide_home_container').css('margin-top', movie_slide);
       },1000);
       setTimeout(function () {
           $('.slide_home_container').removeClass('selected');
           $('.item_slide:nth-child('+next_slide+')').addClass('active');
       },1700);
       index_active+= 1;
   });
   $('.btn-prev').on('click',function () {
      var movie_slide = height_slide - (index_active * height_slide) + height_slide;
      var prev_slide = index_active - 1;
       if(index_active <= 1){
           $(this).addClass('disable');
           return false;
       }else if(index_active <= num_slide){
           $('.btn-next').removeClass('disable')
       }
       $('.slide_home_container').addClass('selected');
       $('.item_slide').removeClass('active');
       setTimeout(function () {
           $('.slide_home_container').css('margin-top', movie_slide);
       },1000);
       setTimeout(function () {
           $('.slide_home_container').removeClass('selected');
           $('.item_slide:nth-child('+prev_slide+')').addClass('active');
       },1700);
       index_active-= 1;
   });

   $('.menu_big_list li').hover(function () {
       $('.menu_big_list li').removeClass('menu_hover');
       $(this).next().addClass('menu_hover');
       $(this).prev().addClass('menu_hover');
   });
   $('.menu_big_list').mouseleave(function () {
      $('.menu_big_list li').removeClass('menu_hover');
   });

   $('.btn_menu').on('click',function () {
      $('.menu_plz').toggleClass('menu_plz_open');
   });
});